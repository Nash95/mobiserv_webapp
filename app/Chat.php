<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','message','initiator','recipient','created_at'
    ];

    public function getUpdatedAtColumn() {
        return null;
    }
    
}
