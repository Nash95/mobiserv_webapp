<?php

namespace App\Concerns;

trait ApiResponse {

    public function apiResponse(string $wrapper,array $data, $statusCode = 200){
        return response()->json([
             $wrapper => $data
           ], $statusCode);
    }
}