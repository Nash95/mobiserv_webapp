<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'internal_task_id', 'created_at'
    ];

    public function getUpdatedAtColumn() 
    {
        return null;
    }

    public function internal_task()
    {
        return $this->belongsTo(InternalTask::class);
    }
}
 