<?php

namespace App;

use App\Traits\BelongsToUser;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use BelongsToUser;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehiclereg','driverStatus','user_id'
    ];

    public function getCreatedAtColumn() {
        return null;
    }

    public function getUpdatedAtColumn() {
        return null;
    }

}
