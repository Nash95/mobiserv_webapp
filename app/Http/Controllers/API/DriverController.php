<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User; 
use App\Task;
use App\Chat;
use App\Location;
use App\Events\MessageSubmitted;
use App\Events\CoordinatesSubmitted;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    public $successStatus = 200;

    public function taskDetails() 
    { 
        $data = Task::where('driveruserid', '=', auth()->user()->id)
                ->join('orders', 'orders.id', '=', 'tasks.orderid')
                ->select('orders.title', 'orders.details', 'orders.deliverylocation', 'tasks.*')
                ->get();

        return response()->json(['taskDetails' => $data], $this->successStatus); 
    }

    public function store(Request $request)
    {
        if ($request->progress !== null){
            $data = Task::updateOrCreate(
                ['driveruserid' => auth()->user()->id],
                ['taskStatus'  => $request->progress]                                          
            );
            return response()->json(['taskDetails' => $data], $this->successStatus); 
        }
        elseif ($request->message !== null) {        
            $data = Chat::updateOrCreate(
                [
                    'message'   => $request->message, 
                    'initiator' => auth()->user()->id,
                    'recipient'  => $request->taskAssignerId, 
                    'created_at'=> now()
                ]                                        
            );
            //Pusher
            $text = $request->message;
            $time = now();
            event(new MessageSubmitted($text,$time)); 
            
            return response()->json(['chatMessages' => $data], $this->successStatus); 

        }
        elseif ($request->latitude !== null) {        
            $data = Location::updateOrCreate(
                ['user_id' => auth()->user()->id],
                [
                    'latitude'   => $request->latitude, 
                    'longitude'  => $request->longitude
                ]                                        
            );            
            //Pusher
            $lat = $request->latitude;
            $long = $request->longitude;
            $time = now();
            event(new CoordinatesSubmitted($lat,$long,$time)); 

            return response()->json(['co-odinates' => $data], $this->successStatus); 

        }
        else{ 
            return response()->json(['error'=>'Failed'], 400); 
        }
    }
    //search chat messages
    public function searchMessages(Request $request){
        
        $assignee = $request -> input('taskAssignee');
        //chatmessages
        $response['chats'] = Chat::
                            orderBy('id')
                            ->join('users', 'chats.initiator', '=', 'users.id')
                            ->select('users.name', 'chats.*')
                            ->where('initiator', '=', $assignee)
                            ->where('recipient', '=', auth()->user()->id)
                            ->orwhere('initiator', '=', auth()->user()->id)
                            ->where('recipient', '=', $assignee)
                            ->get();   

        return response()->json($response);   
    }
}
