<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductCategory;

class SupplierController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        return response()->json(['allProductsCategories' => ProductCategory::with('product')->get()], $this->successStatus);
    }

    public function productDetails() 
    {                
        $data = Product::where('user_id', '=', auth()->user()->id)
                ->get();

        return $this->apiResponse('productDetails', $data, 201);
       // return response()->json(['productDetails' => $data], $this->successStatus);    
    }

    public function store(Request $request)
    {
        
        $data = Product::updateOrCreate(
            [   'id' => $request->product_id],
            [
                'user_id' => auth()->user()->id,
                'title'  => $request->title,   
                'description'  => $request->description,   
                'price'  => $request->price,   
                'location'  => $request->location,   
                'categories'  => $request->categories,   
                'image_url'  => $request->image_url
            ]                                       
        );
        return response()->json(['productDetails' => $data], $this->successStatus); 
        
    }
    /**
     * Remove the product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if($product)
            $product->delete(); 
        else
            return response()->json(error);
        return response()->json('product deleted');
        }
    }
