<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\SaveRequest;

//Additionals
use App\User;
use App\Driver;
use App\Department;
use Redirect,Response;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['users'] = User::orderBy('id','desc')
            ->join('departments', 'departments.id', '=', 'users.departmentid')
            ->whereNotIn('departments.id', array(1,auth()->user()->id))
            ->select('departments.name as deptName', 'users.*')
            ->get();
        
        $data['departments'] = Department::whereNotIn('id', array(1,6,7,8))->get(); 
        
        return view('accounts',$data);
    }
    
    public function store(SaveRequest $request)
    {  
        $this->authorize('create', User::class);
       
        if ($request->departmentid == 'Select Department')
        {
            return response()->json(['error'=>'Invalid DepartmentID'], 422);
        }
        elseif ($request->departmentid == null){

        
            $request->departmentid = 6;
            $this->saveUser($request);
        }
        else
        {
            // $departmentid = $request->departmentid;
            $this->saveUser($request);
        }

        
        
    }

    private function saveUser(SaveRequest $request){
        $user = User::updateOrCreate(
            ['id' => $request->user_id],
            [   
                'name' => $request->name, 
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'address' => $request->address,
                'phone' => $request->phonenumber,
                'departmentid'  => $request->departmentid,
                'status'  => "Active"
            ]
        );
        return Response::json($user);
    }
    
    public function edit($id)
    {   
        $where = array('id' => $id);
        // $user  = User::where($where)->first();
 
        if (Driver::where('user_id', '=', $where )->exists()) {
            $user  = User::where($where)->first()
            ->join('drivers', 'drivers.user_id', '=', 'users.id')
            ->select('drivers.vehiclereg', 'users.*')
            ->where('users.id', '=', $id)
            ->get();
        }
        else
            $user  = User::where($where)->first();
        
        return Response::json($user);
    }
 
    public function destroy($id)
    {
        $user = User::where('id',$id)->delete();
   
        return Response::json($user);
    }
    
    //Profile
    public function getProfile()
    {
        $data['users'] = User::
            join('departments', 'departments.id', '=', 'users.departmentid')
            ->select('departments.name as deptName', 'users.*')
            ->where('users.id', '=', auth()->user()->id)
            ->get();

        return view('profile',$data);
    }

    public function storeUpdate(Request $request)
    {  
        if ($request->file('file') == null){
            $user = User::updateOrCreate(
                ['id' => $request->userID],
                [       
                    'name' => $request->user_name,
                    'firstname' => $request->first_name,
                    'lastname'   => $request->last_name, 
                    'phone' => $request->phone,
                    'address'  => $request->address
                ]
            );
        }
        else{

            $docname = $request->file('file')->getClientOriginalName(); 
            $filename = pathinfo($docname, PATHINFO_FILENAME); 
            $extension = $request->file('file')->getClientOriginalExtension();
            $nameToStore = $filename.'.'.$extension;
            $path = $request->file('file')->storeAs('public/avatar',$nameToStore);
            $user = User::updateOrCreate(
                ['id' => $request->userID], 
                [       
                    'avatar' => $nameToStore,
                ]
            );
        }
        
        return Response::json($user);
    }

    
}
