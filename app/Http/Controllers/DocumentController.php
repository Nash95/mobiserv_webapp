<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Additionals
use App\User;
use App\InternalTask;
use App\Document;
use Redirect,Response;
use Auth;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['documents'] = Document::orderBy('id','desc')

        ->join('internal_tasks', 'documents.internal_task_id', '=', 'internal_tasks.id')
        ->select( 'documents.*')
        ->where('internal_tasks.assignerid', '=',  auth()->user()->id)
        ->get();

        return view('documents',$data);
    }

    public function searchDocuments(Request $request){
        $taskID = $request -> input('taskID');
        $response['documents'] = Document::where('taskid', '=', $taskID)
                                ->get();   
        return Response::json($response);   
    }

    public function downloadFile(Request $request){

        $file_name = $request->input('docName');
        print_r($file_name);

        $file = storage_path()."/app/upload/".$file_name;

        $headers = [
            'Content-Type'=> 'application/pdf',
        ];       
        return Response::download($file, $file_name, $headers);
        
    }
}
