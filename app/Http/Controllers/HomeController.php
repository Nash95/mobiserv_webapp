<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MessageSubmitted;

class HomeController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    //Pusher
    public function pusher()
    {
        return view('pusher');
    }
    public function pusherSender()
    {
        return view('pusherSender');
    }
    public function postMessages()
    {  

        $text = request()->text;
        event(new MessageSubmitted($text));
    }
        
}
