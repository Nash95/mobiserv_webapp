<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Additionals
use App\User;
use App\InternalTask;
use App\Chat;
use App\Document;
use App\Events\MessageSubmitted;
use Redirect,Response;
use Auth;
use Illuminate\Support\Facades\Storage;

class InternalTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['internal_tasks'] = InternalTask::orderBy('id','desc')

        ->join('users', 'internal_tasks.assigneeid', '=', 'users.id')
        ->select('users.name', 'internal_tasks.*')
        ->where('internal_tasks.assignerid', '=',  auth()->user()->id)
        ->get();

        // $assignee['users'] = User::all('name','id'); 
        $data['users'] = User::whereNotIn('departmentid', array(1,6,7,8))
                                ->where('id','!=', auth()->user()->id)
                                ->get('name','id'); 

        return view('internalTasks',$data);
    }
    
    public function store(Request $request)
    {  
        if ($request->taskStatus !== null){        
            $data = InternalTask::updateOrCreate(
                ['id' => $request->task_id],
                ['title' => $request->title , 'details'     => $request->details
                                            , 'taskStatus'  => $request->taskStatus
                                            , 'assignedtime'=> now()
                                            , 'assignerid'  => auth()->user()->id
                                            , 'assigneeid'  => $request->assigneeid]
                                        
            );

        return Response::json($data);
        }

        elseif ($request->message !== null) {        
            $data = Chat::create(
                [
                    'message'   => $request->message, 
                    'initiator' => auth()->user()->id,
                    'recipient' => $request->chatRecepient, 
                    'created_at'=> now()
                ]                                        
            );
            //Pusher
            $text = $request->message;
            $time = now();
            event(new MessageSubmitted($text,$time));    
            
        return Response::json($data);        

        }

        else {  
            if ($request->file('file')) {
                // your code here
            
            $docname = $request->file('file')->getClientOriginalName(); 
            // print_r("The docname is ");
            // print_r($docname);
            $filename = pathinfo($docname, PATHINFO_FILENAME); 
            $extension = $request->file('file')->getClientOriginalExtension();
            $nameToStore = $filename.'.'.$extension;
            $path = $request->file('file')->storeAs('upload',$nameToStore);
            // print_r($path);

            $data = Document::create(
                [
                    'filename'          => $nameToStore,
                    'internal_task_id'  => $request->taskID,
                    'created_at'        => now()
                ]                                        
            );
            return Response::json("Success");
            }
        }

    }
    
    public function edit($id)
    {   
        $where = array('id' => $id);
        $data  = InternalTask::where($where)->first();
 
        return Response::json($data);
    }

    //seacrch chat messages
    public function searchMessages(Request $request){
        
        $assignee = $request -> input('chatRecepient');
        //chatmessages
        $response['chats'] = Chat::
                            join('users', 'chats.initiator', '=', 'users.id')
                            ->select('users.name', 'chats.*')
                            ->where('initiator', '=', $assignee)
                            ->where('recipient', '=', auth()->user()->id)
                            ->orwhere('initiator', '=', auth()->user()->id)
                            ->where('recipient', '=', $assignee)
                            ->get();   

        return Response::json($response);   
    }

    public function searchDocuments(Request $request){
        $taskID = $request -> input('taskID');
        $response['documents'] = Document::where('internal_task_id', '=', $taskID)
                                ->get();   

        return Response::json($response);   
    }

    public function download($file_name) 
    {
        $file = storage_path()."/app/upload/".$file_name;
        if (!$file_name || !$file) {
            abort(404);
        }
        return response()->download($file);
    }

}
