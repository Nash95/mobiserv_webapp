<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Additionals
use App\User;
use App\Chat;
use App\Order;
use App\Task;
use App\Driver;
use App\Events\MessageSubmitted;
use Redirect,Response;
use Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['orders'] = Order::orderBy('id','desc')

        ->join('users', 'orders.user_id', '=', 'users.id')
        ->select('users.name', 'orders.*')
        ->get();

        $data['drivers'] = Driver::orderBy('drivers.id', 'asc')
        ->join('users', 'drivers.user_id', '=', 'users.id')
        ->select('users.name','users.id As driveruserid', 'drivers.*')
        ->where('driverStatus', '=', 'Waiting')
        ->get();     
        return view('requests',$data);
    }
       
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        if ($request->driveruserid !== null){  
            if($request->order_status !== 'Approved'){                    
                $data = Task::updateOrCreate(
                    [    
                        'taskStatus'  => '0'
                        , 'created_at'  => now()
                        , 'driveruserid'    => $request->driveruserid
                        , 'orderid'     => $request->order_id
                        , 'taskAssignerId'     => auth()->user()->id
                    ]                                        
                );
                $data = Driver::updateOrCreate(
                    ['user_id' => $request->driveruserid],
                    ['driverStatus'  => 'Active']                                        
                );
                $data = Order::updateOrCreate(
                    ['id' => $request->order_id],
                    ['orderStatus'  => 'Approved']                                        
                );
            }
            else{
                $data = 'approved_already';
            }
        }
        elseif ($request->denyOrder !== null){
            if ($request->order_status !== 'Approved'){            
                $data = Order::updateOrCreate(
                    ['id' => $request->order_id],
                    ['orderStatus'  => 'Denied']                                        
                );
            }
            else{
                $data = "denied";
            }
        }

        else {        
            $data = Chat::updateOrCreate(
                [
                    'message'   => $request->message, 
                    'initiator' => auth()->user()->id,
                    'recipient' => $request->taskAssignee, 
                    'created_at'=> now()
                ]                                        
            );
            //Pusher
            $text = $request->message;
            $time = now();
            event(new MessageSubmitted($text,$time));            

        }
        return Response::json($data);
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $where = array('id' => $id);
        $data  = Order::where($where)->first();
 
        return Response::json($data);
    }

    //seacrch chat messages
    public function searchMessages(Request $request){
        
        $assignee = $request -> input('taskAssignee');
            //chatmessages
           $response['chats'] =    Chat::where('initiator', '=', $assignee)
                               ->where('recipient', '=', auth()->user()->id)
                               ->orwhere('initiator', '=', auth()->user()->id)
                               ->where('recipient', '=', $assignee)
                               ->get();   

        return Response::json($response);   
    }

}
