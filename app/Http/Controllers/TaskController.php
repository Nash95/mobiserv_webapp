<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Additionals
use App\User;
use App\Chat;
use App\Order;
use App\Task;
use App\Driver;
use App\Location;
use App\Events\MessageSubmitted;
use Redirect,Response;
use Auth;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            
        $data['tasks'] = Task::orderBy('id','desc')

        ->join('orders', 'tasks.orderid', '=', 'orders.id')
        ->join('users', 'tasks.driveruserid', '=', 'users.id')
        ->select('orders.title', 'orders.details', 'users.name', 'tasks.*')
        ->where('tasks.taskAssignerId', '=', auth()->user()->id)
        ->get();
        return view('tasks',$data);
    }
       
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        if ($request->message == null){        
            $data = Task::updateOrCreate(
                [    
                      'taskStatus'  => '0'
                    , 'created_at'  => now()
                    , 'driverid'    => $request->driver_id
                    , 'orderid'     => $request->order_id
                ]
                                        
            );
            $data = Order::updateOrCreate(
                ['id' => $request->order_id],
                ['orderStatus'  => 'Approved']                                        
            );
        }

        else {        
            $data = Chat::updateOrCreate(
                [
                    'message'   => $request->message, 
                    'initiator' => auth()->user()->id,
                    'recipient' => $request->taskAssignee, 
                    'created_at'=> now()
                ]                                        
            );
            //Pusher
            $text = $request->message;
            $time = now();
            event(new MessageSubmitted($text,$time));            

        }
        return Response::json($data);
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $where = array('id' => $id);
        $data  = Order::where($where)->first();
 
        return Response::json($data);
    }

    //seacrch chat messages
    public function searchMessages(Request $request){
        
        $assignee = $request -> input('taskAssignee');
            //chatmessages
           $response['chats'] =    Chat::where('initiator', '=', $assignee)
                               ->where('recipient', '=', auth()->user()->id)
                               ->orwhere('initiator', '=', auth()->user()->id)
                               ->where('recipient', '=', $assignee)
                               ->get();   

        return Response::json($response);   
    }

    //GetCoordinates
    public function coords(Request $request)
    {   
        $where = array('user_id' => $request->driverUserID);
        $data  = Location::where($where)->first();
 
        return Response::json($data);
    }
}
