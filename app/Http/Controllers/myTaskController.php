<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Additionals
use App\User;
use App\InternalTask;
use App\Chat;
use App\Events\MessageSubmitted;
use Redirect,Response;
use Auth;

class myTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $data['internal_tasks'] = InternalTask::orderBy('id','desc')
        
        ->join('users', 'internal_tasks.assignerid', '=', 'users.id')
        ->select('users.name', 'internal_tasks.*')
        ->where('internal_tasks.assigneeid', '=',  auth()->user()->id)
        ->get();

        $data['users'] = User::all('name','id');     
        return view('myTasks',$data);
    }
       
    public function store(Request $request)
    {  

        if ($request->slider !== null){
            $data = InternalTask::updateOrCreate(
                ['id' => $request->task_id],
                ['taskStatus'  => $request->slider]                                          
            );
        }       
       
        return Response::json($data);
    }
    
    public function edit($id)
    {   
        $where = array('id' => $id);
        $messenger  = Chat::where($where)->first();
 
        return Response::json($data);
    }
    
}
