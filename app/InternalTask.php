<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalTask extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','title','details','taskStatus','assignerid','assigneeid'
    ];

    public function getUpdatedAtColumn() 
    {
        return null;
    }
     /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'assignedtime';

    public function document()
    {
        return $this->hasMany(Document::class);
    }
    
}
