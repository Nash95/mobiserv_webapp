<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class DriverObserver
{
    /**
     * Handle the user "creating" event.
     *
     * @param User $user
     *
     * @return void
     */
    public function creating(User $user)
    {
        if ( Str::of(URL::current())->contains('accounts') ) {         
            $user->password = Hash::make('init01');
        }
    }

    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if ( request()->filled('vehiclereg') && Str::of(URL::current())->contains('accounts') ) {         
            $user->driver()->updateOrCreate([
                'driverStatus'  => "Waiting"
                ], request()->only([
                    'vehiclereg'
                ]
            ));
        }
        
    }

    public function saved(User $user)
    {
        if ( request()->filled('vehiclereg') && Str::of(URL::current())->contains('accounts') ) {         
            $user->driver()->updateOrCreate([
                'user_id'=>$user->id
            ],
                request()->only([
                    'vehiclereg'
            ]));
        }
        
    }

    
}
