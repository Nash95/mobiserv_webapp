<?php

namespace App;

use App\Traits\BelongsToUser;
use App\ProductCategory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use belongsToUser;
    /**
     * Links this model to a product_category.
     *
     * @return mixed
     */
    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','description','price','image_url','product_category_id','location','user_id'
    ];
}
