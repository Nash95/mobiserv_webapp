<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'taskStatus','created_at','driveruserid','orderid', 'taskAssignerId'
    ];

    public function getUpdatedAtColumn() {
        return null;
    }
}
