<?php

namespace App\Traits;

use App\User;

trait BelongsToUser
{
    /**
     * Links this model to a user.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
