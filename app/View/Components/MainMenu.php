<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MainMenu extends Component
{
    public $externalMenuItems = [
        '/requests'         =>'Orders current',
        '/tasks'            =>'Tasks',
    ];
    public $internalMenuItems = [
        '/internalTasks'    =>'Plan_Desk',
        '/myTasks'          =>'Assigned_To_me',
        '/documents'        =>'My Dual Share Drive',
    ];
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.main-menu');
    }
}
