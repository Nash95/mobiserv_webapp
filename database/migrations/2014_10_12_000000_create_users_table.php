<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->text('firstname')->nullable();
            $table->text('lastname')->nullable();
            $table->text('phone')->nullable();
            $table->text('address')->nullable();
            $table->integer('level')->nullable();
            $table->text('status')->nullable();
            $table->text('avatar')->nullable();
            $table->timestamps();
            
            $table->bigInteger('departmentid')->unsigned()->nullable();     
            $table->foreign('departmentid')->references('id')->on('departments')->onDelete('cascade');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
