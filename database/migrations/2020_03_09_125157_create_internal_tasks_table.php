<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('details');
            $table->string('taskStatus');
            $table->timestamp('assignedtime')->useCurrent();
            $table->integer('assignerid');
            $table->integer('assigneeid');
            $table->string('attachments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_tasks');
    }
}
