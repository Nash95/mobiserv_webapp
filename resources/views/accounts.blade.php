@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Accounts<a href="#" target="_blank" >->Users</a></h2><br>
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2 addDriver" id="create-new-driver">Add Driver</a> 
          <a href="javascript:void(0)" class="btn btn-success mb-2 addStaff" id="create-new-staff">Add Staff</a>

          <table class="table table-bordered" id="user_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>First Name</th>
                 <th>Last Name</th>
                 <th>Email</th>
                 <th>Phone Number</th>
                 <th>Address</th>
                 <th>Status</th>
                 <th>Join Date</th>
                 <th>Department</th>
                 
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="users-crud">
              @foreach($users as $u_info)
              <tr id="user_id_{{ $u_info->id }}">
                 <td>{{ $u_info->id  }}</td>
                 <td>{{ $u_info->firstname }}</td>
                 <td>{{ $u_info->lastname }}</td>
                 <td>{{ $u_info->email }}</td>
                 <td>{{ $u_info->phone }}</td>
                 <td>{{ $u_info->address }}</td>
                 <td>{{ $u_info->status }}</td>
                 <td>{{ $u_info->created_at }}</td>
                 <td>{{ $u_info->deptName }}</td>
                 <td colspan="2">
                    <a href="javascript:void(0)" id="edit-user" data-id="{{ $u_info->id }}" class="btn btn-info ">Edit</a>
                    <a href="javascript:void(0)" id="delete-user" data-id="{{ $u_info->id }}" class="btn btn-danger delete-user">Delete</a>
                  </td>
              </tr>
              @endforeach
           </tbody>
          </table>
       </div> 
    </div>
</div>

<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="userCrudModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal">
          <div class="modal-body">
              <input type="hidden" name="user_id" id="user_id">
              <!-- <input type="hidden" name="departmentid" id="departmentid"> -->

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">UserName</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required>
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">FirstName</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter First Name" value="" maxlength="50" required>
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">LastName</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter Last Name" value="" maxlength="50" required>
                </div>
              </div>
 
              <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-12">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">PhoneNumber</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="Enter Phone Number" value="" maxlength="50" required>
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="" maxlength="50" required>
                </div>
              </div>

              <div class="form-group" id ="departmentTag">   
                <label class="col-sm-2 control-label">Department</label> 
                <div class="col-sm-12">
                  <select class="form-control" name="departmentid" id="departmentid">
                    <option>Select Department</option>

                    @foreach ($departments as $department)
                      <option value="{{ $department->id }}" > {{ $department->name }}</option>
                    @endforeach    
                  </select>
                </div>
              </div>

              <div id="vehicle-data">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">VehicleReg</label>
                  <div class="col-sm-12">
                      <input type="text" class="form-control" id="vehiclereg" name="vehiclereg" placeholder="Enter Reg Num" value="" maxlength="50" required>
                  </div>
                </div>
              </div>

          </div>
      
          <div class="modal-footer">
            <span class="input-group-btn">
              <a href="javascript:void(0)" class="btn btn-success" id="btn-save">Save changes</a>
            </span>
          </div> 

        </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*  When user click add driver button */      
    $('body').on('click', '.addDriver', function () {  
        $('#btn-save').val("create-user");     
        $('#userForm').trigger("reset");
        $('#user_id').attr('disabled','disabled');
        $('#userCrudModal').html("Add New Driver");
        $('#btn-save').html("Save Driver");
        $('#email').removeAttr('readonly');
        $('#vehicle-data').fadeIn();
        $('#departmentTag').fadeOut();
        $('#vehiclereg').removeAttr('disabled');
        $('#departmentid').attr('disabled','disabled');
        $('#ajax-crud-modal').modal('show');
    
    });

    /*  When user click add staff button */      
    $('body').on('click', '.addStaff', function () {  
        $('#btn-save').val("create-user");     
        $('#userForm').trigger("reset");
        $('#userCrudModal').html("Add Staff Member");
        $('#btn-save').html("Save Staff");
        $('#email').removeAttr('readonly');
        $('#departmentid').removeAttr('disabled');
        $('#vehicle-data').fadeOut();
        $('#departmentTag').fadeIn();
        $('#vehiclereg').attr('disabled','disabled');
        $('#ajax-crud-modal').modal('show');
    
    });
 
    /* When click edit user */
    $('body').on('click', '#edit-user', function () {
        var user_id = $(this).data('id');
        var response; 

        $.get('accounts/' + user_id +'/edit', function (response) {
          if (response[0] == undefined){
              data = response;
              $('#vehicle-data').fadeOut();
              $('#departmentTag').fadeIn();
          }
          else{
            $('#departmentTag').fadeOut();
            $('#vehicle-data').fadeIn();
            data = response[0];
          }
          
            $('#btn-save').val("edit-user");
            $('#userForm').trigger("reset");
            $('#userCrudModal').html("Edit User");
            $('#btn-save').html("Save User");
            $('#ajax-crud-modal').modal('show');
            $('#user_id').val(data.id);
            $('#name').val(data.name);
            $('#firstname').val(data.firstname);
            $('#lastname').val(data.lastname);
            $('#email').val(data.email);
            $('#email').attr('readonly','readonly');
            $('#vehiclereg').removeAttr('readonly');
            $('#vehiclereg').removeAttr('disabled');
            $('#departmentid').removeAttr('disabled');
            $('#phonenumber').val(data.phone);
            $('#address').val(data.address);
            $('#vehiclereg').val(data.vehiclereg);
            $('#departmentid').val(data.departmentid);
        });

     });


   $('#btn-save').click(function () { 
      
      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');      
      var message = $("#departmentid").val();
      $.ajax({
          data: $('#userForm').serialize(),
          url: "{{ url('accounts')}}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var user  = '<tr id="user_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.firstname + '</td><td>' 
                          + data.lastname + '</td><td>' + data.email + '</td><td>' + data.phone + '</td><td>' + data.address 
                          + '</td><td>' + data.status + '</td><td>' + data.created_at + '</td><td>' + data.departmentid +'<td>';
                  user += '<td colspan="2"><a href="javascript:void(0)" id="edit-user" data-id="' + data.id + '" class="btn btn-info ">Edit</a>';
                  user += '<a href="javascript:void(0)" id="delete-user" data-id="' + data.id + '" class="btn btn-danger delete-user ">Delete</a></td></tr>';
                    
              

              if (actionType == "create-user") {
                  $('#users-crud').prepend(user);
              } else {
                  $("#user_id_" + data.id).replaceWith(user);  
              }        
                 
              $('#userForm').trigger("reset");              
              $('#ajax-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {

              if (data.status == 422){
                alert("Please select a valid department")
                $('#userForm').trigger("reset");              
                $('#ajax-crud-modal').modal('hide');
                $('#btn-save').html('Save Changes');
              }
              location.reload();

              
          }
      });

    });


   //delete user login
    $('body').on('click', '.delete-user', function () {
        var user_id = $(this).data("id");
        if(confirm("Are You sure want to delete !")) {
 
        $.ajax({
            type: "DELETE",
            url: "{{ url('accounts')}}"+'/'+user_id,
            success: function (data) {
                $("#user_id_" + user_id).remove();
            },
            error: function (data) {

            }
        });
       }
    }); 

  });
 
 
   
  
</script>
    
@endsection