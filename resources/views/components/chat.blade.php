<div>
    <!-- I begin to speak only when I am certain what I will say is not better left unsaid - Cato the Younger -->
   <script>
    //chat modal
   $('body').on('click', '.toChat', function () {    

      //for Internal chats
      $('#message').fadeIn();
      $('#chatForm').trigger("reset");
      $('#modal_title').html("Chat Usr");
      $("#create-new-document").prop('id', 'create-new-message');
      $('#create-new-message').html("Send");
      $('#file').fadeOut();
      //for Internal chats end

      var element = document.getElementById("totalTodos");
      var chatRecepient = $(this).data("id");
      $('#recipient').val(chatRecepient);
      console.log(chatRecepient)
      $('#chat-modal').modal('show');
      checkMessages();
      setTimeout(function(){ element.scrollTop = element.scrollHeight; },500);

      function checkMessages(){
        $.ajax({
            data: {chatRecepient: chatRecepient},
            type: "GET",
            url: "{{ url('./chatmessages')}}",
            dataType: 'json',
            success: function (response) {
                var chats=response.chats;

                function compare( a, b ) {
                  if ( a.created_at < b.created_at ){
                    return -1;
                  }
                  if ( a.created_at > b.created_at ){
                    return 1;
                  }
                  return 0;
                }
                
                chats.sort( compare );

                var trHTML="";

                for(var i=0;i<chats.length;i++){
                    var time=chats[i].created_at;
                    var message=chats[i].message;
                    var initiator=chats[i].initiator;
                    var recipient=chats[i].recipient;
                    var name=chats[i].name;
                    let trRow="<div class='direct-chat-msg right'>"+
                                "<div class='direct-chat-info clearfix'><span class='direct-chat-timestamp pull-right'>"+time+"</span></div>"+
                                "<div class='direct-chat-info clearfix'><span class='direct-chat-timestamp pull-right'>"+name+"</span></div>"+
                                "<div class='direct-chat-text'>"+message+"</div>"
                                +"</div>"+
                            "</div>";
                trHTML=trHTML+trRow;
                }
                $('#totalTodos').empty();
                $('#totalTodos').append(trHTML); 
                
            },
            error: function (err) {
            }
        });
       }

    }); 
    //chat modal end

    //Pusher Listener

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = false;

    var pusher = new Pusher('3fcfb679d13d85df6fca', {
    cluster: 'eu',
    forceTLS: true
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('form-submitted', function(data) {
        // alert(JSON.stringify(data));
        var message = data.text;
        var time = data.time;
        var element = document.getElementById("totalTodos");

        let trRow="<div class='direct-chat-msg right'>"+
                                "<div class='direct-chat-info clearfix'><span class='direct-chat-timestamp pull-right'>"+time+"</span></div>"+
                                "<div class='direct-chat-text'>"+message+"</div>"
                                +"</div>"+
                            "</div>";
        $('#totalTodos').append(trRow);         
        element.scrollTop = element.scrollHeight;

    });
    //Pusher Listener end

    // send message        
    $('#create-new-message').click(function () {
        $('#create-new-message').html('Sending..');
        var sendTo = $('#recipient').val();
        var message = $("#message").val();
        $.ajax({
            data: { chatRecepient: sendTo,
                    message: message
                },
            type: "POST",
            url: "{{ url('internalTasks')}}",
            dataType: 'json',
            success: function (response) { 
                
                $('#create-new-message').html('Send'); 
                console.log(response)                 
                var element = document.getElementById("totalTodos");
                // element.scrollTop = element.scrollHeight;
                // checkMessages();
                $('#message').val('');                             
            },
            error: function (err) {
                console.log(err)
            }
        });
    });
    //end send message

    </script>

</div>