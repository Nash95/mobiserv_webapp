<div>
    <script>
    //Document modal
    $('body').on('click', '.toEdit', function () {

          
        $('#file').fadeIn();
        $('#chatForm').trigger("reset");
        $('#userForm').trigger("reset");
        $('#modal_title').html("Documents");
        $("#create-new-message").prop('id', 'create-new-document');
        $('#create-new-document').html("UploadDoc");
        $('#message').fadeOut();

        var taskID = $(this).data("id");   
        $('#taskID').val(taskID);  
        $('#chat-modal').modal('show');
        checkDocuments();   

        function checkDocuments(){
            $.ajax({
                data: {taskID: taskID},
                type: "GET",
                url: "{{ url('./checkDocuments')}}",
                dataType: 'json',
                success: function (response) {
                    var documents=response.documents;
                    var trHTML="";

                    for(var i=0;i<documents.length;i++){
                        var docID=documents[i].id;
                        var docTitle=documents[i].filename;
                        var download = "Download";
                        let trRow=  "<div class='direct-chat-msg right'>"+
                                    "<div class='direct-chat-text'>"+docTitle+"</div>"+
                                    "<a class='btn btn direct-chat-timestamp pull-right' href='download/"+docTitle+ "'>Download</a>"+
                                    "</div>";


                    trHTML=trHTML+trRow;
                    }
                    $('#totalTodos').empty();
                    $('#totalTodos').append(trHTML); 
                    
                },
                error: function (err) {
                }
            });
        }

    });

    // send document 
    $('body').on('click', '#create-new-document', function () {
    // $('#create-new-document').click(function () {
        $('#create-new-document').html('Uploading..');
        var doc = new FormData(chatForm) ; 
        var files = $('#file')[0].files[0];
        var taskID = $('#taskID').val();
        doc.append('file', files);
        doc.append('taskID', taskID);
        $.ajax({
            data: doc,                    
            type: "POST",
            url: "{{ url('internalTasks')}}",
            dataType: 'json',
            contentType: false, 
            processData: false,
            success: function (data) {  
                $('#create-new-document').html('Upload More');   
                $('#message').val('');   
                alert("Document Uploaded Successfully")          
                var element = document.getElementById("totalTodos");
                                            
            },
            error: function (err) {
            }
        });


    });
    //end send document 
    </script>
</div>