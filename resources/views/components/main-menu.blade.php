<div>

    <li class="nav-header">EXTERNAL TASKS (CRM)</li>
    <li class="nav-item has-treeview"> 
          
        <ul class="nav nav-treeview-active">            
            @foreach($externalMenuItems as $link => $text)
                <li class="nav-item">
                    <a href="{{ $link }}"
                        @if(\Illuminate\Support\Str::of(\Illuminate\Support\Facades\URL::current())->contains($link))
                        class='nav-link active'
                        @else
                        class='nav-link'
                        @endif > 
                        <i class="far fa-circle nav-icon"></i>
                        <p>{{ $text }}</p>
                    </a>                    
                </li>
            @endforeach
        </ul>
    </li>

    <li class="nav-item  ">
        <a href="#" class="nav-link ">
            <i class="nav-icon fas fa-tree"></i>
            <p>
            Suppliers
            </p>
        </a>           
    </li>

    <li class="nav-header">INTERNAL TASKS</li>
    <li class="nav-item has-treeview">   
        <ul class="nav nav-treeview-active">            
            @foreach($internalMenuItems as $link => $text)
                <li class="nav-item">
                    <a href="{{ $link }}"
                        @if(\Illuminate\Support\Str::of(\Illuminate\Support\Facades\URL::current())->contains($link))
                        class='nav-link active'
                        @else
                        class='nav-link'
                        @endif > 
                        <i class="far fa-circle nav-icon"></i>
                        <p>{{ $text }}</p>
                    </a>                    
                </li>
            @endforeach
        </ul>
    </li>
    
</div>

