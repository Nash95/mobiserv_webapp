@extends('layouts.admin')

@section('content')
    <br><br>
    <div class="Container-fluid">
        <div class="col-md-12">
            <!-- USERS LIST -->
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">My Documents</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button>
                </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                 <ul class="users-list clearfix">
                   @foreach( $documents as $doc)
                    <li>
                    <img class="profile-user-img img-fluid img-circle" src="{{ asset('storage/default.png') }}" alt="Doc">
                    <a class='users-list-name' href='/download/{{ $doc->filename }}'>{{ $doc->filename }}</a>
                    <span class="users-list-date">{{ $doc->created_at->format('D d M Y H:i') }}</span>
                    </li>
                   @endforeach                  
                 </ul>
                 <!-- /.users-list -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                <a href="javascript::">Docs</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!--/.card -->
        </div>
        <!-- /.col -->
    </div>

@endsection