
@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Tasks assigned by me</h2><br>
    <div class="row">
        <div class="col-12">
        <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-user">Assign Task</a> 
          
        <div class="card">

          <div class="card-body table-responsive p-0" style="height: 678px;">
          <!-- Table -->
            <div id="userstable">
            @include('partials/assignedByMe')
            </div>
          <!-- Table End -->
          
          </div>
        </div>  
       </div> 
    </div>
</div>

<div class="modal fade" id="task-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="taskModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal" enctype="multipart/form-data">
          <div class="modal-body">
              <input type="hidden" name="task_id" id="task_id">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" maxlength="50" required="">
                </div>
              </div>
 
              <div class="form-group">
              <label class="col-sm-2 control-label">Details</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="details" name="details" placeholder="Enter Details" value="" required="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="taskStatus" name="taskStatus" value="0" hidden>
                </div>
              </div>

              <div class="form-group" id ="assigneeTag">   
                <label class="col-sm-2 control-label">Assignee</label> 
                <div class="col-sm-12">
                  <select class="form-control" name="assigneeid" id="assigneeid">
                    <option>Select Assignee</option>

                    @foreach ($users as $user)
                      <option value="{{ $user->id }}" > {{ $user->name }} </option>
                    @endforeach    
                  </select>
                </div>
              </div>

          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary" id="btn-save" value="create">
                Save changes
              </button>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- Modal Chat -->
<div id="chat-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-body">

            <!-- DIRECT CHAT PRIMARY -->
            <div class="box box-primary  direct-chat-primary" >
                <div class="box-header with-border">
                    <h2 id="modal_title" class="box-title"><b>chatUser</b></h2>

                    <div class="box-tools pull-right">
                    <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                        <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <input id ="userID" value ="" hidden>
                    </div>
                </div>
                <!-- /.box-header -->

                <!-- chatBody start -->
                <div id="totalTodos" style = "overflow: auto; height:400px ">0</div>
                               
                <!-- /.chatBody end -->          


                <!-- /.box-body -->
                <div class="box-footer">
                    <form id ="chatForm" enctype="multipart/form-data">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="recipient" name="recipient" value="" hidden>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="taskID" name="taskID" value="" hidden>
                            </div>
                        </div>

                        <div class="input-group"> 

                            <!-- additional for doc upload -->
                            <input type="file" id = "file" name="file" class="form-control"/>  

                            <input type="text" id = "message" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-message">Send</a>
                            </span>

                        </div>
                    </form>
                </div>
                <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
        
        </div>
        
    </div>

    </div>
</div>
<!-- /Modal Chat End-->

<!-- Chat Logic -->
<x-chat/>

<!-- Document Logic -->
<x-file-upload/>

<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*  When user click add user button */
    $('#create-new-user').click(function () {
        $('#btn-save').val("create-user");
        $('#userForm').trigger("reset");
        $('#taskModal').html("Assign Task");
        $('#task-modal').modal('show');
        $('#assigneeTag').fadeIn();
    });    

  });    
 
 if ($("#userForm").length > 0) {
      $("#userForm").validate({
 
     submitHandler: function(form) {
      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');
      
      $.ajax({
          data: $('#userForm').serialize(),
          url: "{{ url('internalTasks')}}", 
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var user = '<tr id="task_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.title + '</td><td>' + data.details + '</td><td>' + data.taskStatus + '</td><td>' + data.assignedtime + '</td><td>' + data.assigneeid + '</td>';
                  user += '<td><a href="javascript:void(0)" id="toChat" data-id="' + data.id + '" class="btn btn-success toChat">Chat</a></td>';
                  user += '<td><a href="javascript:void(0)" id="edit-user" data-id="' + data.id + '" class="btn btn-info toEdit">Edit</a></td></tr>';
               
               
              
              if (actionType == "create-user") {
                  $('#users-crud').prepend(user);
              }
              else if (actionType == "upload-new-document") {
                  alert("Document saved");
              }
              else {
                  $("#task_id_" + data.id).replaceWith(user);
              }
 
              $('#userForm').trigger("reset");
              $('#task-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
   
  
</script>
    
@endsection