@extends('layouts.admin')

@section('content')
    
<div class="container">
    <h2 style="margin-top: 12px;" class="alert alert-success">Tasks Assigned to me</h2><br>
    <div class="row">
        <div class="col-12">

          <div class="box-body table-responsive no-padding">
          <table class="table table-bordered" id="laravel_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Title</th>
                 <th>Details</th>
                 <th>Progress</th>
                 <!-- <th>Complete%</th> -->
                 <th>Assigned Time</th>
                 <th>Assigner</th>
                 
                 
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="users-crud">
              @foreach($internal_tasks as $t_info)
              <tr id="task_id_{{ $t_info->id }}">
                 <td>{{ $t_info->id  }}</td>
                 <td>{{ $t_info->title }}</td>
                 <td>{{ $t_info->details }}</td>
                 <td><span class="btn badge bg-blue toUpdate" id="{{ $t_info->taskStatus }}" name="{{ $t_info->id }}">{{ $t_info->taskStatus }} %</span></td>
                 <!-- <td>{{ $t_info->taskStatus }}</td> -->
                 <td>{{ $t_info->assignedtime->format('D d M Y H:i') }}</td>
                 <td>{{ $t_info->name }}</td>
                 <td colspan="2">
                    <a href="javascript:void(0)" id="toChat" data-id="{{ $t_info->assignerid }}" class="btn btn-success toChat">Chat</a>
                    <a href="javascript:void(0)" id="edit-user" data-id="{{ $t_info->id }}" class="btn btn-info mr-2 toEdit">Edit</a>
                  </td>
              </tr>
              @endforeach
           </tbody>
          </table>
          </div>
          
       </div> 
    </div>
</div>

<div class="modal fade" id="task-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="taskModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal">
          <div class="modal-body">

              <!-- w3schools -->
              <div class="slidecontainer">
               <label class="col-sm-2 control-label">Update Progress</label>
               <input type="range" min="1" max="100" value="" class="slider" id="myRange">
              </div>

          </div>
          <div class="modal-footer">
            <span class="input-group-btn">
              <a href="javascript:void(0)" class="btn btn-success mb-2" id="saveProgress">Save Progress</a>
            </span>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- Modal Chat -->
<div id="chat-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-body">

            <!-- DIRECT CHAT PRIMARY -->
            <div class="box box-primary  direct-chat-primary" >
                <div class="box-header with-border">
                    <h2 id="modal_title" class="box-title"><b>chatUser</b></h2>

                    <div class="box-tools pull-right">
                    <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                        <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <input id ="userID" value ="" hidden>
                    </div>
                </div>
                <!-- /.box-header -->

                <!-- chatBody start -->
                <div id="totalTodos" style = "overflow: auto; height:400px ">0</div>
                
                <!-- /.box-body -->
                <div class="box-footer">
                    <form id ="chatForm">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="recipient" name="recipient" value="" hidden>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="taskID" name="taskID" value="" hidden>
                            </div>
                        </div>

                        <div class="input-group">    

                            <!-- additional for doc upload -->
                            <input type="file" id = "file" name="file" class="form-control"/> 

                            <input type="text" id = "message" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-message">Send</a>
                            </span>

                        </div>
                    </form>
                </div>
                <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
        
        </div>
        
    </div>

    </div>
</div>
<!-- /Modal Chat End-->

<!-- Chat Logic -->
<x-chat/>

<!-- Document Logic -->
<x-file-upload/>

<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

     
  });

    //Slider range modal
    $('body').on('click', '.toUpdate', function () {
        
        var initprogValue = $(this).attr('id');
        var progTaskID = $(this).attr('name');
        
        $('#task-modal').modal('show');
        $('#myRange').val(initprogValue);        

        $('#saveProgress').click(function () {

            var slider = $("#myRange").val();
            $.ajax({
                data: {   
                        slider: slider,
                        task_id: progTaskID
                    },                 
                type: "POST",
                url: "{{ url('myTasks')}}",
                dataType: 'json',
                success: function (data) {
                    location.reload();                            
                },
                error: function (err) {
                }
            });
        });

    });
    //Slider range modal end   
  
</script>
    
@endsection