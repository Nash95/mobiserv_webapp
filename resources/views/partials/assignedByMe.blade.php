<table class="table table-head-fixed text-nowrap" id="laravel_crud">
    <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Details</th>
            <th>Progress</th>
            <!-- <th>Complete%</th> -->
            <th>Assigned Time</th>
            <th>Assignee</th>
            <th>Chat</th>
            <th>Documents</th>
            
        </tr>
    </thead>
    <tbody id="users-crud">
        @foreach($internal_tasks as $t_info)
        <tr >
            <td>{{ $t_info->id  }}</td>
            <td>{{ $t_info->title }}</td>
            <td>{{ $t_info->details }}</td>
            <td><span class="badge bg-blue">{{ $t_info->taskStatus }} %</span></td>
            <!-- <td>{{ $t_info->taskStatus }}</td> -->
            <td>{{ $t_info->assignedtime->format('D d M Y H:i') }}</td>
            <td>{{ $t_info->name }}</td>
            <td>
                <a href="javascript:void(0)" id="toChat" data-id="{{ $t_info->assigneeid }}" class="btn btn-success toChat">Chat</a>                    
            </td>
            <td>
                <a href="javascript:void(0)" id="edit-user" data-id="{{ $t_info->id }}" class="btn btn-info mr-2 toEdit">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>