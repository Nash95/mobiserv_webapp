@extends('layouts.admin')

@section('content')

        <!-- /.content -->
	<section class="content">
     <div class="container-fluid">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        User Profile
	      </h1>
	    </section>

	    <!-- Main content -->
	    <section class="content">

	      <div class="row">
	        <div class="col-md-3">

            @foreach($users as $user)
	          <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="{{ asset('storage/avatar/'.$user->avatar) }}"
                                alt="profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{ $user->name  }}</h3>

                        <p class="text-muted text-center">Mobiserv Staff</p>

                        <form id="avatarform" method="post" enctype="multipart/form-data" >

                            <input type="text" id="taskid" name = "taskid" value="" hidden /> 
                            <input type="file" id="file" name="file" class="form-control" /><br><br>
                            <button class="btn btn-primary btn-block toSaveAvatar" type = "button" id="btn" name = "avt">Save</button>
                                            
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
              <!-- Profile Image End -->

	          <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-book mr-1"></i> Join Date</strong>

                        <p class="text-muted">
                            {{ $user->created_at->format('D d M Y H:i') }}
                        </p>

                        <hr>

                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                        <p class="text-muted">{{ $user->address }}</p>

                        <hr>
                    
                        <hr>

                        <hr><br><br><br><br><br>

                    </div>
                    <!-- /.card-body -->
                </div>
              <!-- About Me Box End -->
	        </div>
	        <!-- /.col -->

            <!-- Details Table -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#" data-toggle="tab">Settings</a></li>
                    </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="settings">

                                <form id="detailsForm" class="form-horizontal">

                                    <input type="text" id="userID" value="{{ $user->id }}" hidden name="">

                                    <div class="form-group">                            
                                        <label class="col-sm-2 control-label">Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="user_name" name="username" value="{{ $user->name }}" required>
                                        </div >
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Firstname</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="first_name" name="firstname" value="{{ $user->firstname }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Lastname</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="last_name" name="lastname" value="{{ $user->lastname }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Address</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="addr" name="address" value="{{ $user->address }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Phone Number</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="p_num" name="phone" value="{{ $user->phone }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="button"  href="#" id="btn" class=" btn btn-primary toSaveUpdate" ><i class="fa fa-save"></i> Save</button>
                                            
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- Details Table End -->


	        </div>
        </div>
	        <!-- /.row -->
            @endforeach

		  <!-- edit the user from db Start-->
		  <script>
            $(document).ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(document).on("click", ".toSaveUpdate", function () {			
                    				
                    var userID =$("#userID").val();
                    var user_name = $("#user_name").val();				
                    var first_name = $("#first_name").val();
                    var last_name= $("#last_name").val();
                    var address= $("#addr").val();
                    var phone= $("#p_num").val();	

                    $.post("{{ url('profileUpdate')}}", { 
                        userID:userID,
                        user_name:user_name,
                        first_name:first_name,
                        last_name:last_name,
                        address:address,
                        phone:phone,
                    },	
                            
                    function(data) {

                        alert("Details Successfully Saved");
                    });

                });

                //Avatar Update
                $(document).on("click", ".toSaveAvatar", function () {	
                    var fd = new FormData(avatarform) ; 
                    var userID =$("#userID").val();
                    var files = $('#file')[0].files[0]; 
                    fd.append('file', files); 
                    fd.append('userID', userID);
                    
                    $.ajax({ 
                        url: '{{ url('profileUpdate')}}', 
                        type: 'post', 
                        data: fd, 
                        contentType: false, 
                        processData: false,     
                        success: function (response) {
                            location.reload();                                                         
                        },
                        error: function (err) {
                        }

                    });
                });

            });
		  </script>
		  <!-- edit the user from db End-->


		

	    </section>
	    <!-- /.content -->
	</section>




@endsection