@extends('layouts.admin')

@section('content')
    <h1>Pusher Test</h1>
    <p>
        Try publishing an event to channel <code>my-channel</code>
        with event name <code>my-event</code>.
    </p>

    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;

        var pusher = new Pusher('3fcfb679d13d85df6fca', {
        cluster: 'eu',
        forceTLS: true
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('form-submitted', function(data) {
        alert(JSON.stringify(data));
        });
    </script>
@endsection

@if(isset($chats))

                @foreach ( $chats  as $c_info)

                    
                    @if( $c_info->initiator == auth()->user()->id ) 
                            
                        <!-- Message. Default to the left -->
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-right">{{ $c_info->created_at }}</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="dist/img/avatar.png" alt=""><!-- /.direct-chat-img -->
                            <div class="direct-chat-text">{{ $c_info->message }}</div>
                            <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->           

                    
                    @else
                        
                        <!-- Message to the right -->
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-left">{{ $c_info->created_at }}</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="" alt=""><!-- /.direct-chat-img -->
                            <div class="direct-chat-text">{{ $c_info->message }}</div>
                            <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                    
                    
                    @endif

                @endforeach
                @endif