@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Customer Requests</h2><br>
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Tasks</h3>

            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
                </div>
            </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 678px;">
            <table class="table table-head-fixed text-nowrap">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Service Details</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Status</th></th>
                    <th>DeliveryTo</th>
                    <th>Chat</th>
                    <th>Edit</th>
                    
                </tr>
                </thead>
                <tbody>
                    @foreach($orders as $t_info)
                    <tr id="order_id_{{ $t_info->id }}">
                        <td>{{ $t_info->id  }}</td>
                        <td>{{ $t_info->title }}</td>
                        <td>{{ $t_info->details }}</td>
                        <td>{{ $t_info->name }}</td>
                        <td>{{ $t_info->created_at->format('D d M Y H:i') }}</td>
                        <td><span class="badge bg-blue">{{ $t_info->orderStatus }}</span></td>
                        <td>{{ $t_info->deliverylocation }}</td>
                        <td>
                            <a href="javascript:void(0)" id="toChat" data-id="{{ $t_info->user_id }}" class="btn btn-success toChat">Chat</a>                    
                        </td>
                        <td >
                            <a href="javascript:void(0)" id="edit-request" data-id="{{ $t_info->id }}" class="btn btn-info mr-2">Edit</a>
                        </td>
                    </tr>
                    @endforeach                  
                </tbody>
            </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
    </div> 
</div>

<div class="modal fade" id="task-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="taskModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal">
          <div class="modal-body">
              <input type="hidden" name="order_id" id="order_id">
              <input type="hidden" name="user_id" id="user_id">
              <input type="hidden" name="order_status" id="order_status">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" maxlength="50" disabled >
                </div>
              </div>
 
              <div class="form-group">
              <label class="col-sm-2 control-label">Details</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="details" name="details" placeholder="Enter Details" value="" disabled>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="taskStatus" name="taskStatus" value="0" hidden>
                </div>
              </div>


              <div class="form-group" id ="assigneeTag">   
                <label class="col-sm-2 control-label">Assignee</label> 
                <div class="col-sm-12">
                  <select class="form-control" name="driveruserid" id="driveruserid">
                    <option>Select Driver</option>

                    @foreach ($drivers as $driver)
                      <option value="{{ $driver->driveruserid }}" > {{ $driver->name }}</option>
                    @endforeach    
                  </select>
                </div>
              </div>


              <a href="javascript:void(0)" class="btn btn-success mb-2" id="btn-deny-request" value="create">Deny</a>
              <button href="javascript:void(0)" type="submit" class="btn btn-primary mb-2 pull-right" id="btn-save" value="create">Approve</button>



          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- Modal Chat -->
<div id="chat-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-body">

            <!-- DIRECT CHAT PRIMARY -->
            <div class="box box-primary  direct-chat-primary" >
                <div class="box-header with-border">
                    <h2 class="box-title"><b>chatUser</b></h2>

                    <div class="box-tools pull-right">
                    <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                        <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <input id ="userID" value ="" hidden>
                    </div>
                </div>
                <!-- /.box-header -->

                <!-- chatBody start -->
                <div id="totalTodos" style = "overflow: auto; height:400px ">0</div>
                               
                <!-- /.chatBody end -->          


                <!-- /.box-body -->
                <div class="box-footer">
                    <form id ="chatForm">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="recipient" name="recipient" value="" hidden>
                            </div>
                        </div>

                    <div class="input-group">                       
                    

                        <input type="text" id = "message" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-btn">

                            <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-message">Send</a>
                            <!-- <button type="button" onclick = "sendmessages()" id = "btn"  class="btn btn-primary btn-flat">&nbsp&nbsp&nbsp&nbsp<i class="fa fa-send">&nbsp&nbsp</i></button> -->
                            </span>

                    </div>
                    </form>
                </div>
                <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
        
        </div>
        
    </div>

    </div>
</div>
<!-- /Modal Chat End-->

<!-- Chat Logic End Start -->
<x-chat/>
<!-- Chat Logic End -->

<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*  When user click deny request button */
    $('#btn-deny-request').click(function () {

        var order_id = $("#order_id").val();
        var order_status = $("#order_status").val();
        $.ajax({
          data: {   denyOrder:      'Denied',
                    order_id:       order_id,
                    order_status:   order_status
                },
          type: "POST",
          url: "{{ url('requests')}}",
          dataType: 'json',
          success: function (response) {
            if (response == "denied"){
                alert('Error Order in transit');
                $('#userForm').trigger("reset");
                $('#task-modal').modal('hide');
            }
            else{
                alert('Order Denied');
                location.reload(); 
            }
                                       
          },
          error: function (err) {

          }
        });

    });
 
   /* When click edit request  */
   $('body').on('click', '#edit-request', function () {

      var order_id = $(this).data('id');      
      $.get('requests/' + order_id +'/edit', function (data) {
         $('#taskModal').html("Edit Task");
          $('#btn-save').val("edit-request");
          $('#task-modal').modal('show');
          $('#order_id').val(data.id);
          $('#title').val(data.title);
          $('#details').val(data.details);
          $('#taskStatus').val(data.taskStatus);
          $('#order_status').val(data.orderStatus);        
          $('#user_id').val(data.user_id)
          $('#assigneeTag').fadeIn();
      })
   });   

  });
 
 if ($("#userForm").length > 0) {
      $("#userForm").validate({          
 
     submitHandler: function(form) {
 
      var actionType = $('#btn-save').val();
      $('#btn-save').html('Posting..');
      
      $.ajax({
          data: $('#userForm').serialize(),
          url: "{{ url('requests')}}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              if (data == "approved_already"){
                alert('Order approved already');
              }
              else {
                alert('Order Approved');
                location.reload();
              }
 
              $('#userForm').trigger("reset");
              $('#task-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              alert("Please Select Driver")
              $('#btn-save').html('Approve');
          }
      });
    }
  })
}
   
  
</script>
    
@endsection