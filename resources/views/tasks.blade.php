@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Tasks</h2><br>
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Tasks</h3>

            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
                </div>
            </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 678px;">
            <table class="table table-head-fixed text-nowrap">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Details</th>
                    <th>Progress</th>
                    <th>Assigned Date</th>
                    <th>Driver</th></th>             
                    
                    <td colspan="2">Chat/Location</td>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $t_info)
                <tr>
                    <td>{{ $t_info->id  }}</td>
                    <td>{{ $t_info->title }}</td>
                    <td>{{ $t_info->details }}</td>
                    <td><span class="badge bg-blue">{{ $t_info->taskStatus }}</span></td>
                    <td>{{ $t_info->created_at->format('D d M Y H:i') }}</td>
                    <td>{{ $t_info->name }}</td>
                    <td colspan="2">
                        <a href="javascript:void(0)" id="toChat" data-id="{{ $t_info->driveruserid }}" class="btn btn-success toChat">Chat</a>                    
                        <a href="javascript:void(0)" id="toLocation" data-id="{{ $t_info->driveruserid }}" class="btn btn-info mr-2 toLocation">Location</a>
                    </td>
                </tr>
                @endforeach                  
                </tbody>
            </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
    </div> 
</div>


<div class="modal fade" id="task-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="taskModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal">
          <div class="modal-body">

              <input type="hidden" name="lat" id="lat" value="-17.824858">
              <input type="hidden" name="long" id="long" value="31.053028">
               
              <div id="map"></div>


          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default close" data-dismiss="modal">
                Close
              </button>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- Modal Chat -->
<div id="chat-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-body">

            <!-- DIRECT CHAT PRIMARY -->
            <div class="box box-primary  direct-chat-primary" >
                <div class="box-header with-border">
                    <h2 class="box-title"><b>chatUser</b></h2>

                    <div class="box-tools pull-right">
                    <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                        <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <input id ="userID" value ="" hidden>
                    </div>
                </div>
                <!-- /.box-header -->

                <!-- chatBody start -->
                <div id="totalTodos" style = "overflow: auto; height:400px ">0</div>
                               
                <!-- /.chatBody end -->          


                <!-- /.box-body -->
                <div class="box-footer">
                    <form id ="chatForm">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="recipient" name="recipient" value="" hidden>
                            </div>
                        </div>

                    <div class="input-group">                       
                    

                        <input type="text" id = "message" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-btn">

                            <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-message">Send</a>
                            <!-- <button type="button" onclick = "sendmessages()" id = "btn"  class="btn btn-primary btn-flat">&nbsp&nbsp&nbsp&nbsp<i class="fa fa-send">&nbsp&nbsp</i></button> -->
                            </span>

                    </div>
                    </form>
                </div>
                <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
        
        </div>
        
    </div>

    </div>
</div>
<!-- /Modal Chat End-->

<!-- Chat Logic End Start -->
<x-chat/>
<!-- Chat Logic End -->

<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //maps
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
    //maps end
 
  });
  
</script>

<!-- maps -->
<script>

    /* When click location button  */
    $('body').on('click', '.toLocation', function initMap() {

        //Pusher Listener

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;
        var pusher = new Pusher('3fcfb679d13d85df6fca', {
        cluster: 'eu',
        forceTLS: true
        });
        var channel = pusher.subscribe('coords-channel');
        var handler = function(){
            checkCoordinates();
        };
        channel.bind('coords-submitted', handler);
        //Pusher Listener End

        var driverUserID = $(this).data('id'); 
        checkCoordinates()
        function checkCoordinates(){          
            $.ajax({
            data: {driverUserID: driverUserID},
            type: "GET",
            url: "{{ url('./coords')}}",
            dataType: 'json',
            success: function (data) {
                
                $('#lat').val(data.latitude);
                $('#long').val(data.longitude);

                $('#taskModal').html("Driver Location");
                $('#btn-save').val("edit-request");
                $('#task-modal').modal('show');
                $('#assigneeTag').fadeIn(); 

                //Drv Cordinates
                var drvlat= $('#lat').val();
                var drvlng= $('#long').val();

                var map, infoWindow;           

                var pos = {
                    lat: parseFloat(drvlat),
                    lng: parseFloat(drvlng)
                };

                map = new google.maps.Map(document.getElementById('map'), {
                center: pos,
                zoom: 16
                });

                infoWindow = new google.maps.InfoWindow;
                infoWindow.setPosition(pos);
                infoWindow.setContent('Driver Location');
                infoWindow.open(map);
                map.setCenter(pos);        
        
                function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                    infoWindow.setPosition(pos);
                    infoWindow.setContent(browserHasGeolocation ?
                                        'Error: The Geolocation service failed.' :
                                        'Error: Your browser doesn\'t support geolocation.');
                    infoWindow.open(map);
                }
                
            },
            error: function (err) {
            }
            }); 
        }    
        
    });

</script> 

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZGScxsQbD6_stdknnI0wcKUHy2eFMqZE&callback=initMap"
    async defer>
</script>

@endsection