<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//OAuth using Passport
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){

    //Driver
    Route::post('taskDetails', 'API\DriverController@taskDetails');
    Route::post('storeDriver', 'API\DriverController@store');
    Route::post('chat', 'API\DriverController@searchMessages');
    Route::post('location', 'API\DriverController@location');
    //Supplier
    Route::post('storeProduct', 'API\SupplierController@store');
    Route::post('productDetails', 'API\SupplierController@productDetails');
    Route::delete('deleteProduct/{id}', 'API\SupplierController@destroy');

});

Route::get('productDetails', 'API\SupplierController@index');
