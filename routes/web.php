<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');

//pusher
Route::get('/pusher', 'HomeController@pusher')->name('pusher');
Route::get('/pusherSender', 'HomeController@pusherSender')->name('pusherSender');
Route::post('/pusherSender', 'HomeController@postMessages')->name('pusherSender');

// Route::get('/try', function() {
//     echo "thula";
// });
Auth::routes();

//Accounts
Route::resource('accounts', 'AccountController');
Route::get('profile', 'AccountController@getProfile');
Route::post('profileUpdate', 'AccountController@storeUpdate');
// Route::post('registerDriver', 'AccountController@registerDriver');

//Internal Tasks
Route::resource('internalTasks', 'InternalTaskController');
Route::get('chatmessages', 'InternalTaskController@searchMessages');
Route::get('checkDocuments', 'InternalTaskController@searchDocuments');
Route::get('download/{file}', 'InternalTaskController@download');

//Internal Tasks Assigned to me
Route::resource('myTasks', 'myTaskController');

Route::resource('chat', 'ChatController');

//External Tasks
Route::resources([  'requests'  =>  'OrderController',
                    'tasks' =>  'TaskController' ]);
Route::get('coords', 'TaskController@coords');

//documents
Route::resource('documents', 'DocumentController');
